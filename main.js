const productData = [
    {
        id: 1,
        title: 'Cà Phê Sữa Đá',
        amount: 29,
        image: 'https://minio.thecoffeehouse.com/image/admin/1639377738_ca-phe-sua-da_400x400.jpg',
        categoryId: 1
    },
    {
        id: 2,
        title: 'Cà Phê Sữa Nóng',
        amount: 35,
        image: 'https://minio.thecoffeehouse.com/image/admin/1639377770_cfsua-nong_400x400.jpg',
        categoryId: 1
    },
    {
        id: 3,
        title: 'Bạc Sỉu',
        amount: 29,
        image: 'https://minio.thecoffeehouse.com/image/admin/1639377904_bac-siu_400x400.jpg',
        categoryId: 1
    },
    {
        id: 4,
        title: 'Cà Phê Đen Đá',
        amount: 29,
        image: 'https://minio.thecoffeehouse.com/image/admin/1639377798_ca-phe-den-da_400x400.jpg',
        categoryId: 1
    },
    {
        id: 5,
        title: 'Latte Táo Lê Quế',
        amount: 59,
        image: 'https://minio.thecoffeehouse.com/image/admin/1642389499_1638267966-latte-tao-le-que-lanh_400x400.jpg',
        categoryId: 1
    },
    {
        id: 6,
        title: 'Trà Sen Nhãn Sum Vầy',
        amount: 59,
        image: 'https://minio.thecoffeehouse.com/image/admin/1642338110_tra-sen-nhan_400x400.jpeg',
        categoryId: 2
    },
    {
        id: 7,
        title: 'Trà Dưa Đào Sung Túc',
        amount: 59,
        image: 'https://minio.thecoffeehouse.com/image/admin/1642336343_tra-dao-dua-luoi_400x400.jpeg',
        categoryId: 2
    },
    {
        id: 8,
        title: 'Trà Đào Cam Sả',
        amount: 45,
        image: 'https://minio.thecoffeehouse.com/image/admin/tra-dao-cam-xa_668678_400x400.jpg',
        categoryId: 2
    },
    {
        id: 9,
        title: 'Hồng Trà Sữa Trân Châu',
        amount: 55,
        image: 'https://minio.thecoffeehouse.com/image/admin/hong-tra-sua-tran-chau_326977_400x400.jpg',
        categoryId: 2
    },
    {
        id: 10,
        title: 'Trà Match Latte Nóng',
        amount: 59,
        image: 'https://minio.thecoffeehouse.com/image/admin/matcha-latte-_936022_400x400.jpg',
        categoryId: 3
    },
    {
        id: 11,
        title: 'Chocolate Nóng',
        amount: 59,
        image: 'https://minio.thecoffeehouse.com/image/admin/chocolatenong_949029_400x400.jpg',
        categoryId: 3
    },
    {
        id: 12,
        title: 'Combo 3 Hộp Cà Phê Sữa Đá Hòa Tan',
        amount: 109,
        image: 'https://minio.thecoffeehouse.com/image/admin/combo-3cfsd-nopromo_320619_400x400.jpg',
        categoryId: 4
    },
    {
        id: 13,
        title: 'Mochi Kem Xoài',
        amount: 19,
        image: 'https://minio.thecoffeehouse.com/image/admin/1643101968_mochi-xoai_400x400.jpg',
        categoryId: 5
    },
    {
        id: 14,
        title: 'Mít Sấy',
        amount: 20,
        image: 'https://minio.thecoffeehouse.com/image/admin/mit-say_666228_400x400.jpg',
        categoryId: 5
    },
    {
        id: 14,
        title: 'Combo 3 Hộp Trà Lài Túi Lọc Tearoma',
        amount: 69,
        image: 'https://minio.thecoffeehouse.com/image/admin/1641440398_1638341675-combo-tra-huong-lai_400x400.jpg',
        categoryId: 6
    },

]
const categoryData = [
    {
        id: 1,
        title: "Cà phê",
        icon: "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/ca-phe.png"
    },
    {
        id: 2,
        title: "Trà trái cây - Trà sữa",
        icon: "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/tra-trai-cay-tra-sua.png"
    },
    {
        id: 3,
        title: "Đá xay - Choco - Matcha",
        icon: "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/da-xa.png"
    },
    {
        id: 4,
        title: "Thưởng thức tại nhà",
        icon: "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/ca-phe-goi-ca-phe-uong-lien.png"
    },
    {
        id: 5,
        title: "Bánh - Snack",
        icon: "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/banh-snack.png"
    },
    {
        id: 6,
        title: "Combo",
        icon: "https://minio.thecoffeehouse.com/image/tch-web-order/category-thumbnails/combo-moi.png"
    },
    
]
window.onload = function(){
    addProducts(productData);
    
    const categoryList = document.getElementById('categoryList');
    categoryData.map(category=>{
        const item = document.createElement('div');
        item.addEventListener('click', filterProduct);
        item.id = category.id
        item.innerHTML = `<img src="${category.icon}"style="max-width:max-content; width:30px; height:30px;"></img> ${category.title}</a>`;
        categoryList.appendChild(item);
    })
}

function filterProduct(event){
    const classList = event.target.classList;
    if(classList.contains('active'))
    {
        event.target.classList.remove('active');
        addProducts(productData);
    }
    else{
        const activeCategory = document.querySelectorAll('#categoryList .active');
        activeCategory.forEach(element => {
            element.classList.remove('active');
        });
        event.target.classList.add('active');
        const id = event.target.id;
        const filtersProducts = productData.filter(item => item.categoryId == id);
        addProducts(filtersProducts);
    }
}
function addProducts(products){
    const productList = document.getElementById('productList');
    productList.innerHTML = '';
    products.map(product=>{
        const card = document.createElement('div');
        card.innerHTML = `
            <div class="card">
              <img
                src="${product.image}"
                class="card-img-top"
                alt="${product.title}"
              />
              <div>
                <h5 class="card-title">${product.title}</h5>
                <p class="card-text"> ${product.amount}.000đ</p>
              </div>
            </div>
        `;
        productList.appendChild(card);
    })
}